ERLC=erlc
ERLCFLAGS=+debug_info

compile:
	erlc $(ERLCFLAGS) -o ebin/ src/*.erl

edit:
	cd src && vim -p \
		tcp_acceptor_supervisor.erl \
		tcp_acceptor.erl \
		tcp_receiver_supervisor.erl \
		tcp_receiver.erl \
		tcp_receiver_event.erl \
		tcp_util.erl \
		tcp_common.hrl
