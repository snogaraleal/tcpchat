-module(tcp_acceptor).
-behaviour(gen_server).
-export([
    init/1,
    handle_call/3,
    handle_cast/2,
    handle_info/2,
    terminate/2,
    code_change/3
]).

-include("tcp_common.hrl").

init(default) -> init(#address{});

init(Address) ->
    Options = [
        list,
        {packet, raw},
        {active, false},
        {ip, Address#address.host}
    ],

    case gen_tcp:listen(Address#address.port, Options) of
        {ok, Socket} ->
            {ok, #server{socket = Socket,
                         address = Address,
                         acceptor = tcp_util:acceptor_start(Socket)}};

        {error, Reason} -> {stop, Reason}
    end.

handle_call(_Request, _From, State) -> {noreply, State}.

handle_cast(_Request, State) -> {noreply, State}.

handle_info({acceptor, {socket, Socket}}, State) ->
    {ok, Process} = supervisor:start_child(tcp_receiver_supervisor, []),
    gen_tcp:controlling_process(Socket, Process),
    gen_server:cast(Process, {socket, Socket}),
    {noreply, State};

handle_info({acceptor, {error, Reason}}, State) -> {stop, Reason, State}.

terminate(_Reason, State) -> gen_tcp:close(State#server.socket).

code_change(_OldVsn, State, _Extra) -> {ok, State}.
