-module(tcp_receiver).
-behaviour(gen_server).
-export([
    init/1,
    handle_call/3,
    handle_cast/2,
    handle_info/2,
    terminate/2,
    code_change/3
]).

-include("tcp_common.hrl").

init(default) -> init(#client{});

init(Client) -> {ok, Client}.

handle_call(_Request, _From, State) -> {noreply, State}.

handle_cast({socket, Socket}, _State) ->
    gen_event:add_handler(tcp_event_manager,
                          {tcp_receiver_event, self()},
                          self()),

    {ok, {Host, Port}} = inet:peername(Socket),

    gen_tcp:send(Socket, io_lib:format("Welcome ~w ~n",
                                       [Host])),
    gen_tcp:send(Socket, io_lib:format("* Set your alias with `@<alias>` ~n",
                                       [])),
    gen_tcp:send(Socket, io_lib:format("* Disconnect by sending `exit` ~n",
                                       [])),

    {noreply, #client{socket = Socket,
                      address = #address{host = Host, port = Port},
                      receiver = tcp_util:receiver_start(Socket)}}.

handle_info({receiver, {message, "exit"}}, State) ->
    gen_event:notify(tcp_event_manager,
                     {exit, State#client.alias, normal}),
    {stop, normal, State};

handle_info({receiver, {message, "crash"}}, State) ->
    {stop, crash, State};

handle_info({receiver, {message, [$\@ | Alias]}}, State) ->
    gen_event:notify(tcp_event_manager,
                     {change, State#client.alias, Alias}),
    {noreply, State#client{alias = Alias}};

handle_info({receiver, {message, Message}}, State) ->
    gen_event:notify(tcp_event_manager,
                     {message, State#client.alias, Message}),
    {noreply, State};

handle_info({receiver, {error, Reason}}, State) ->
    gen_event:notify(tcp_event_manager,
                     {exit, State#client.alias, Reason}),
    {stop, Reason, State};

handle_info({event, {left, Alias}}, State) ->
    gen_tcp:send(State#client.socket,
                 io_lib:format("> ~s left ~n", [Alias])),
    {noreply, State};

handle_info({event, {exit, Alias, Reason}}, State) ->
    gen_tcp:send(State#client.socket,
                 io_lib:format("> ~s disconnected (~s) ~n", [Alias, Reason])),
    {noreply, State};

handle_info({event, {change, Alias, NewAlias}}, State) ->
    gen_tcp:send(State#client.socket,
                 io_lib:format("> ~s changed alias to ~s ~n",
                               [Alias, NewAlias])),
    {noreply, State};

handle_info({event, {message, Alias, Message}}, State) ->
    gen_tcp:send(State#client.socket,
                 io_lib:format("~s: ~s ~n", [Alias, Message])),
    {noreply, State}.

terminate(_Reason, State) ->
    gen_event:delete_handler(tcp_event_manager,
                             {tcp_receiver_event, self()},
                             []),
    gen_tcp:close(State#client.socket).

code_change(_OldVsn, State, _Extra) -> {ok, State}.
