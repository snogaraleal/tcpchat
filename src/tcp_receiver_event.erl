-module(tcp_receiver_event).
-behaviour(gen_event).
-export([
    init/1,
    handle_event/2,
    handle_call/2,
    handle_info/2,
    terminate/2,
    code_change/3
]).

init(Owner) -> {ok, {owner, Owner}}.

handle_event({exit, Alias, normal}, {owner, Owner}) ->
    Owner ! {event, {left, Alias}},
    {ok, {owner, Owner}};

handle_event({exit, Alias, Reason}, {owner, Owner}) ->
    Owner ! {event, {exit, Alias, Reason}},
    {ok, {owner, Owner}};

handle_event({change, Alias, NewAlias}, {owner, Owner}) ->
    Owner ! {event, {change, Alias, NewAlias}},
    {ok, {owner, Owner}};

handle_event({message, Alias, Message}, {owner, Owner}) ->
    Owner ! {event, {message, Alias, Message}},
    {ok, {owner, Owner}}.

handle_call(_Request, State) -> {ok, ok, State}.

handle_info(_Info, State) -> {ok, State}.

terminate(_Reason, _State) -> ok.

code_change(_OldVsn, State, _Extra) -> {ok, State}.
