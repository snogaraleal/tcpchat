-module(tcp_receiver_supervisor).
-behaviour(supervisor).
-export([
    init/1
]).

init(_Args) ->
    Flags = {simple_one_for_one, 0, 1},
    Children = [
        {
            tcp_receiver,
            {gen_server, start_link, [tcp_receiver, default, []]},
            temporary,
            infinity,
            worker,
            []
        }
    ],
    {ok, {Flags, Children}}.
