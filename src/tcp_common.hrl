-define(DEFAULT_HOST, {127, 0, 0, 1}).
-define(DEFAULT_PORT, 8000).
-define(DEFAULT_ALIAS, "anon").

-record(address, {host = ?DEFAULT_HOST, port = ?DEFAULT_PORT}).
-record(handler, {socket::port(), owner::pid()}).
-record(server, {socket::port(), address::inet:ip_address(), acceptor::pid()}).
-record(client, {socket::port(), address, receiver::pid(),
                 alias=?DEFAULT_ALIAS::string()}).
