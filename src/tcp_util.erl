-module(tcp_util).

-export([
    strip/1
]).

-export([
    acceptor_start/1,
    acceptor_loop/1
]).

-export([
    receiver_start/1,
    receiver_loop/1
]).

-include("tcp_common.hrl").

strip(Content) ->
    re:replace(Content, "(^\\s+)|(\\s+$)", "", [global, {return, list}]).

acceptor_start(Socket) when is_port(Socket) ->
    acceptor_start(#handler{socket = Socket, owner = self()});

acceptor_start(Handler) ->
    proc_lib:spawn_link(?MODULE, acceptor_loop, [Handler]).

acceptor_loop(Handler) ->
    case gen_tcp:accept(Handler#handler.socket) of
        {ok, Socket} ->
            Handler#handler.owner ! {acceptor, {socket, Socket}},
            acceptor_loop(Handler);

        {error, Reason} ->
            Handler#handler.owner ! {acceptor, {error, Reason}}
    end.

receiver_start(Socket) when is_port(Socket) ->
    receiver_start(#handler{socket = Socket, owner = self()});

receiver_start(Handler) ->
    proc_lib:spawn_link(?MODULE, receiver_loop, [Handler]).

receiver_loop(Handler) ->
    case gen_tcp:recv(Handler#handler.socket, 0) of
        {ok, Message} ->
            Handler#handler.owner ! {receiver, {message, strip(Message)}},
            receiver_loop(Handler);

        {error, Reason} ->
            Handler#handler.owner ! {receiver, {error, Reason}}
    end.
