-module(tcp_acceptor_supervisor).
-behaviour(supervisor).
-export([
    init/1
]).

init(_Args) ->
    Flags = {one_for_one, 0, 1},
    Children = [
        {
            tcp_event_manager,
            {gen_event, start_link, [{local, tcp_event_manager}]},
            permanent,
            infinity,
            worker,
            []
        },

        {
            tcp_acceptor,
            {gen_server, start_link, [tcp_acceptor, default, []]},
            permanent,
            infinity,
            worker,
            [tcp_acceptor]
        },

        {
            tcp_receiver_supervisor,
            {supervisor, start_link, [{local, tcp_receiver_supervisor},
                                      tcp_receiver_supervisor,
                                      []]},
            permanent,
            infinity,
            supervisor,
            []
        }
    ],
    {ok, {Flags, Children}}.
